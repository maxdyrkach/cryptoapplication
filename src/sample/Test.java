package sample;

import javax.crypto.CipherInputStream;
import java.io.*;
import java.util.Arrays;

/**
 * Created by Макс on 08.11.2017.
 */


public class Test {
    static Cryptor cryptor = new Cryptor();

    public static void main (String args[]) throws IOException {
        File file = new File("e:\\12.encrypted");
        int offset = cryptor.findOffsetOfHandle(file);
        System.out.println (offset);
        char[] sBuf=new char[512];
        BufferedReader reader = new BufferedReader (new  InputStreamReader(new FileInputStream(file),"unicode"));
        reader.skip(offset);
        reader.read(sBuf);
        System.out.println(Arrays.toString(sBuf));
    }
}
