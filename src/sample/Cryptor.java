package sample;

import com.sun.istack.internal.Nullable;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.Scanner;

public class Cryptor {

    private SecretKey key;
    public volatile double currentVal = 0;
    public volatile double maxVal = 1;

    //шифрует файл
    public File encrypt(File inputFile, @Nullable File outputFile, String sKey) throws Exception {


        if (outputFile == null || outputFile.getName().trim().equals("")) {
            outputFile = new File(inputFile.getParent(), inputFile.getName() + ".encrypted");
        }

        if (outputFile.exists()) {
            outputFile.delete();
            outputFile.createNewFile();
        }



        long inputFileLength = inputFile.length();

        int bufSize;
        if (inputFileLength <= 1024)
            bufSize = 128;
        else if (inputFileLength > 1024 * 1024 * 512)
            bufSize = 1024 * 1024 * 32;
        else
            bufSize = (int) inputFileLength / 20;


        String algorithm = "AES";
        byte[] keyStrict = sKey.getBytes();
        keyStrict = Arrays.copyOf(keyStrict, 16);
        //sKey=Arrays.toString(keyStrict);

        key = new SecretKeySpec(keyStrict, algorithm);

        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, key);

        //Date date = new Date();
        //SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss");

//        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outputFile), "Unicode");
//        String string = "!!ENCRYPTED!!" + inputFile.getName() + "$$$";
//        writer.write(string);
//        writer.close();

        byte[] sBuf = new byte[bufSize];
        FileInputStream fis = new FileInputStream(inputFile);
        BufferedInputStream inStream = new BufferedInputStream(fis);
        CipherOutputStream cipherOS = new CipherOutputStream(new FileOutputStream(outputFile.getPath(), true), cipher);
        BufferedOutputStream outStream = new BufferedOutputStream(cipherOS);


        int count;
        currentVal=0;
        maxVal = inStream.available();

        do {
            count = inStream.read(sBuf);
            if (count != -1) {
                outStream.write(sBuf, 0, count);
                currentVal = currentVal+count;
                //progress.setProgress(totalCount / available);
            }
        } while (count != -1);

        //SecretKeyFactory skf = SecretKeyFactory.getInstance(algorithm);
        //AESKeySpec keyspec = (DESedeKeySpec) skf.getKeySpec(key, DESedeKeySpec.class);
        //fos.write(key.getKey());
        // fos.close();
        outStream.close();
        inStream.close();

        return outputFile;
    }

    public File decrypt(File cryptedFile, String sKey) throws Exception {




        String algorithm = "AES";
        byte[] keyStrict = sKey.getBytes();
        keyStrict = Arrays.copyOf(keyStrict, 16);
        key = new SecretKeySpec(keyStrict, algorithm);

        long cryptedFileLength = cryptedFile.length();
        int bufSize;
        if (cryptedFileLength <= 1024)
            bufSize = 128;
        else {
            if (cryptedFileLength > 1024 * 1024 * 512)
                bufSize = 1024 * 1024 * 32;
            else
                bufSize = (int) (cryptedFileLength / 20);
        }



        String outFileName = cryptedFile.getName().replace(".encrypted", ".decrypted");
        File decryptedFile = new File(cryptedFile.getParent(), outFileName);
        if (decryptedFile.exists()) {
            decryptedFile.delete();
            decryptedFile.createNewFile();
        }
        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.DECRYPT_MODE, key);


            byte[] sBuf = new byte[bufSize];
            BufferedInputStream inStream = new BufferedInputStream(new CipherInputStream(new FileInputStream(cryptedFile), cipher));
            //ObjectInputStream inStream = new ObjectInputStream(new CipherInputStream(new FileInputStream(cryptedFile), cipher));

            BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(decryptedFile, true));

//            int offset = findOffsetOfHandle(cryptedFile);
//            inStream.skip(offset);
            //String secret; // = (String) inStream.read();

            int count;
            double totalCount=0;
            double available = inStream.available();

            do {
                count = inStream.read(sBuf);
                if (count != -1) {
                    outStream.write(sBuf, 0, count);
                    totalCount += count;
                    //progress.setProgress(totalCount / available);
                }
            } while (count != -1);

//            while (inStream.available() > 0) {
//                int count = inStream.read(sBuf);
//                //if (count != -1)
//                outStream.write(sBuf, 0, count);
//            }

//            int i = inStream.read(sBuf);
//            outStream.write(sBuf, 0, i);
//            if (i != -1) {
//                while (inStream.read() != -1) {
//                    int j = inStream.read(sBuf);
//                    outStream.write(sBuf, 0, j);
//                }
//            }

            inStream.close();
            outStream.close();
        } catch (InvalidKeyException e) {
            System.err.println("InvalidKeyException!");
        }
       /*catch (IOException e){
            System.err.println("IOException!");
       }*/
        return decryptedFile;
    }

    public static boolean isEncrypted(File file) {
        try {
            Scanner sc = new Scanner(new InputStreamReader(new FileInputStream(file), "unicode"));
            if (sc.findInLine("!!ENCRYPTED!!") != null)
                return true;
            else
                return false;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;

    }

    int findOffsetOfHandle(File file) throws IOException {
        char[] buf = new char[512];
        int offset = -1;
        String str = new String();
        InputStreamReader reader = new InputStreamReader(new FileInputStream(file), "unicode");
        reader.read(buf);
        for (char i : buf)
            str = str.concat(String.valueOf(i));
        offset = str.indexOf("$$$") + 3;
        if (offset > 3)
            return offset;
        else
            return -1;
    }


}
