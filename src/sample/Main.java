package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {




    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader mainLoader = new FXMLLoader();
        mainLoader.setLocation(getClass().getResource("MainScene.fxml"));
        Parent root = mainLoader.load();
        MainController mainController = mainLoader.getController();
        mainController.setMainStage(primaryStage);
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setTitle("Easy Crypting App");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();




    }


    public static void main(String[] args) {
        launch(args);
    }
}
