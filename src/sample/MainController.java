package sample;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;


public class MainController implements Initializable {

    private Stage processingStage;
    private Stage mainStage;
    private FXMLLoader processingLoader = new FXMLLoader();
    private Parent processingParent;
    private ProcessingController processingController;


    @FXML
    private Button chooseFileBtn, chooseOutputFileBtn, decryptBtn, encryptBtn, chooseFileDecryptBtn;
    @FXML
    private TextField inputFileTextField, outputFileTextField, decryptFileTextField;
    @FXML
    private PasswordField passwordField;

    @FXML
    TableView table;
    @FXML
    Label infoPane;




    private int limit = 16;

    private File inputFile;
    private File outputFile;
    private File cryptedFile;
    private Cryptor cryptor = new Cryptor();

    //сделать переменные для последних директорий
    private File lastActiveInputDir;
    private File lastActiveOutDir;
    private File lastActiveDecriptDir;

    private Task<Void> task;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        initLoader();
    }

    private void initLoader() {
        try {
            processingLoader.setLocation(getClass().getResource("ProcessingScene.fxml"));
            processingParent = processingLoader.load();
            processingController = processingLoader.getController();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void pressChooseInputFile(ActionEvent event)  {
        final FileChooser chooseInputFile = new FileChooser();
        String pathname  = inputFile != null? inputFile.getParent(): lastActiveInputDir != null?
                lastActiveInputDir.getPath(): System.getProperty("user.home");
        chooseInputFile.setInitialDirectory(new File(System.getProperty("user.home")));
        chooseInputFile.setTitle("Выбрать файл для шифрования:");
        inputFile = chooseInputFile.showOpenDialog(mainStage);
        if(inputFile != null) {
            inputFileTextField.setText(inputFile.getPath());
            lastActiveInputDir = new File(inputFile.getParent());
        }
    }

    public void pressChooseOutputFile(ActionEvent event) {
        final FileChooser chooseOutputFile = new FileChooser();
        String pathname  = inputFile != null? inputFile.getParent(): lastActiveOutDir != null?
                lastActiveOutDir.getPath(): System.getProperty("user.home");
        chooseOutputFile.setInitialDirectory(new File(pathname));
        chooseOutputFile.setTitle("Создать зашифрованный файл:");
        chooseOutputFile.getExtensionFilters().add(new FileChooser.ExtensionFilter("encrypted", "*.encrypted"));
        outputFile = chooseOutputFile.showSaveDialog(mainStage);
        if(outputFile != null) {
            outputFileTextField.setText(outputFile.getPath());
            lastActiveOutDir = new File(outputFile.getParent());
        }
    }

    public void pressChooseDecryptFile(ActionEvent event) {
        final FileChooser chooseFile = new FileChooser();
        String pathname = lastActiveDecriptDir != null ? lastActiveDecriptDir.getPath() : System.getProperty("user.home");
        chooseFile.setInitialDirectory(new File(pathname));
        chooseFile.setTitle("Выбрать файл для расшифровки:");
        chooseFile.getExtensionFilters().add(new FileChooser.ExtensionFilter("encrypted", "*.encrypted"));
        cryptedFile = chooseFile.showOpenDialog(mainStage);
        if (cryptedFile != null/* && Cryptor.isEncrypted(cryptedFile)*/) { //если файл !=0 и зашифрован этой программой
            decryptFileTextField.setText(cryptedFile.getPath());
            lastActiveDecriptDir = new File (cryptedFile.getParent());
        }
//        else {
//            outputFile = null;
//            infoPane.setText("Файл не зашифрован!");
//            Alert fileNotEncryptedAlert = new Alert(Alert.AlertType.INFORMATION);
//            fileNotEncryptedAlert.setTitle("Не расшифровать!");
//            fileNotEncryptedAlert.setHeaderText("Не расшифровать!");
//            fileNotEncryptedAlert.setContentText("Файл поврежден\nили\nне зашифрован этой программой!");
//            fileNotEncryptedAlert.showAndWait();
//        }

    }

    public void enterPassword(ActionEvent event) {

        if (passwordField.getText().length() > limit) {
            passwordField.setText(passwordField.getText().substring(0, limit));
        }
    }

    public void enterChFileName(ActionEvent event) {
        //if (new File (inputFileTextField.getText()).isFile()){
        try {
            inputFile = new File(inputFileTextField.getText());
            System.out.println(inputFile.toString());
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Не файл");
            alert.setHeaderText("Не файл!");
            alert.setContentText("Не файл");
            alert.showAndWait();
        }
    }

    //public void

    public String enterFileNameToTextField(ActionEvent event, TextField textField) {
        return "0";

    }

    public void encrypt(ActionEvent event) throws Exception {

//        String inputFileString = inputFileTextField.getText();
//        System.out.println(inputFileString);
        if(inputFileTextField.getText().trim().isEmpty()){
            try {
                inputFile = new File(inputFileTextField.getText());
            } catch (Exception e) {
                System.err.println("Cannot init inputFile!");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Внимание!");
                alert.setHeaderText("Проверьте имя шифруемого файла!");
                alert.setContentText("Возможно, адрес шифруемого файла неверен.");
                alert.showAndWait();
            }
        }

        if (outputFileTextField.getText().trim().isEmpty())
            outputFile = null;
        else {
            try {
                outputFile = new File(outputFileTextField.getText());
            } catch (Exception f) {
                System.err.println("Cannot init outputFile!");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Внимание!");
                alert.setHeaderText("Проверьте имя результирующего файла!");
                alert.setContentText("Возможно, адрес результирующего файла неверен.");
                alert.showAndWait();
            }
        }




        if (passwordField.getText() != null) {

            processingController.setTask(task);
            processingController.setActionFile(outputFile);
            Scene scene = new Scene(processingParent,489,65);
            processingStage = new Stage();
            processingStage.setScene(scene);
            processingStage.initModality(Modality.WINDOW_MODAL);
            processingStage.initOwner(mainStage);
            processingStage.show();
            task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {

                    while (getProgress() != 1){
                        updateProgress(cryptor.currentVal, cryptor.maxVal);
                    }
                    return null;
                }
            };
            processingController.progressBar.progressProperty().bind(task.progressProperty());
            new Thread(task).run();
            outputFile = cryptor.encrypt(inputFile, outputFile, passwordField.getText());




            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss");
            FileWriter st = new FileWriter(outputFile.getParent() + "/password " + inputFile.getName() + " " + format.format(date) + ".txt");
            st.write("Encryption: " + inputFile.getName() + "\n on " + format.format(date) + "\n Password: " + passwordField.getText());
            st.close();
            infoPane.setText("Зашифровано");

            processingStage.close();
        } else {
            infoPane.setText("Не заполнен пароль!");
        }
    }

    public void decrypt(ActionEvent event) throws Exception {
        //outputFile=new File("e://decrypted.aif");
        cryptor.decrypt(cryptedFile, passwordField.getText());
        infoPane.setText("Расшифровано");
    }







    public void setMainStage (Stage stage){
        this.mainStage = stage;
    }



    public void exit() {
        Platform.exit();
    }
}
