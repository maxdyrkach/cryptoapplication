package sample;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;


import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class ProcessingController implements Initializable {

    private Task<Void> task;
    private File actionFile;



    @FXML
    public ProgressBar progressBar;
    @FXML
    private Button cancelButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }

    public void setTask (Task<Void> task){
        this.task = task;
    }

    public void setActionFile (File file){
        this.actionFile = file;
    }

    public void pressCancelBtn (ActionEvent event){
        if (task.isRunning()) {
            task.cancel(true);
            if (actionFile != null) {
                actionFile.delete();
            }
        }
    }





}
